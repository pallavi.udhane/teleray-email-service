var express = require('express'),
    nodemailer = require('nodemailer'),
    express = require('express'),
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'),
    logger = require('../config/logger'),
    config = require('config'),
    path = require('path'),
    templatesDir = path.resolve(__dirname, '..', 'views/mailer'),
    emailTemplates = require('email-templates'),
    app = express(),
    router = express.Router();
    
router.use(bodyParser.urlencoded({
    extended: true
}))
router.use(methodOverride(function(req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}))

var EmailAddressRequiredError = new Error('email address required');



var baseUrl;
if (config.has('medirazerbaseurl')) {
    baseUrl = config.get('medirazerbaseurl');
    console.log('getting base url as :' + baseUrl);
    logger.info('Inside sendEmail :: getting baseUrl as :' + baseUrl);
}

var defaultTransport = nodemailer.createTransport('SMTP', {
    service: 'Gmail',
    auth: {
        user: config.mailer.auth.user, // Your email id
        pass: config.mailer.auth.pass // Your password
    }
});

router.post('/sendEmailNotification', function(req, res) {
    logger.info('Inside sendEmailNotification');
    console.log('Inside sendEmailNotification');

    var templateName = req.body.templateName;
    var locals = {
        email: req.body.email,
        subject: req.body.subject,
        name: req.body.name,
        resetUrl: req.body.resetUrl,
        username: req.body.username
    };
    logger.info('getting templatename as ::' + templateName + 'locals as ::' + JSON.stringify(locals));
    sendOne(templateName, locals, function(err, responseStatus, html, text) {
        console.log('calling sendOne function');
        if (!err) {
            res.json({
                header: {
                    statuscode: 200,
                    statusmessage: "success"
                },
                data: {
                    message: "Mail successfully sent"
                }
            });
        } else {
            res.json({
                header: {
                    statuscode: 400,
                    statusmessage: "failure"
                },
                data: {
                    errormessage: "There is problem in sending mail.Please try after some time..."
                }
            });
        }

    });
});

var sendOne = function(templateName, locals, fn) {

    console.log('Inside sendone function');
    logger.info('Inside sendOne function');

    // make sure that we have an user email
    if (!locals.email) {
        return fn(EmailAddressRequiredError);
    }
    // make sure that we have a message
    if (!locals.subject) {
        return fn(EmailAddressRequiredError);
    }
    emailTemplates(templatesDir, function(err, template) {
        if (err) {
            console.log('getting error in sendOne ::emailTemplates' + err);
            logger.info('getting error in sendOne ::emailTemplates' + err);
            return fn(err);
        }
        // Send a single email
        template(templateName, locals, function(err, html, text) {
            if (err) {
                console.log('getting error in sendOne ::template ' + err);
                logger.info('getting error in sendOne ::template ' + err);
                return fn(err);
            }
            /*  // if we are testing don't send out an email instead return
              // success and the html and txt strings for inspection
              if (process.env.NODE_ENV === 'test') {
                return fn(null, '250 2.0.0 OK 1350452502 s5sm19782310obo.10', html, text);
              }*/
            var transport = defaultTransport;
            transport.sendMail({
                from: config.mailer.defaultFromAddress,
                to: locals.email,
                subject: locals.subject,
                html: html,
                // generateTextFromHTML: true,
                text: text
            }, function(err, responseStatus) {
                if (err) {
                    return fn(err);
                }
                return fn(null, responseStatus.message, html, text);
            });
        });
    });
}

module.exports = router;