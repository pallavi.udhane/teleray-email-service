var winston = require('winston');
var config = require('config');
require('winston-daily-rotate-file');
var logfilepath;
if (config.has('logfilepath')) {
    logfilepath = config.get('logfilepath');
}

var transport = new winston.transports.DailyRotateFile({
    filename: logfilepath,
    //filename: '../loggs/logs',
    // filename: '/home/teleray_app/logs/logs',
    datePattern: 'yyyy-MM-dd.',
    prepend: true,
    level: process.env.ENV === 'local' ? 'debug' : 'info'
});

var logger = new(winston.Logger)({
    transports: [
        transport
    ]
});

module.exports = logger;